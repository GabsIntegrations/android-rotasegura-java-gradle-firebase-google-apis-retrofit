# RotaSegura #

Este é um aplicativo de Rotas (GPS) onde é possível visualizar uma de 3 melhores rotas com menor indicação de crimes.
Uma lógica é realizada através do site http://www.ondefuiroubado.com.br/ onde é possível obter os tipos de crimes e quantidades
para que possamos traçar a melhor rota para o usuário.

#### Este app é capaz de realizar: ####
* Pesquisa de melhor rota com menor indice de crimes
* Dar a opção de três rotas diferentes
* Informar e auto-selecionar a melhor rota

### Este é um repositório privado. Apenas pessoas autorizadas podem realizar alterações. ###

* Android RotaSegura
* v0.01

### O que precisamos ter em mente antes de iniciar as configurações? ###

* 1 - Usário e senha (credenciais)

### Contribua conosco ###

* Escreva melhorias tanto no framework como na estrutura dos testes
* A nossa política de code review está em construção

### Entre em contato ###

* Owner/Admin - Gabriel Aguido Fraga


## 1 - Usário e senha (credenciais) ##

É possivel criar suas credenciais pelo próprio app, ou através da integração com redes sociais.
Todas estas informações são armazenadas no banco de dados em tempo real Firebase.